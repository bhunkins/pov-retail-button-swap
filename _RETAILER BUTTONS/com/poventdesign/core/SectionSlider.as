/** SECTION MANAGER
 * ----------------------------------------------------------------------------------------------------------
 * @ description:	handles the loading of swfs & transitions for content
 * @ usage:			new SectionManager ( swfHolder:Object, transHolder:Object );
 * 					loadSection (sectionNum:uint);
 * @ note:			sectionNum refers to the panelArray index found in SiteSettings
 * ---------------------------------------------------------------------------------------------------------- */

package com.poventdesign.core
{
	import com.greensock.TweenNano;
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	import com.poventdesign.loader.SWFLoader;
	import com.poventdesign.ui.video.VideoPlayer;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	import src.SiteSettings;

	
	public class SectionSlider extends MovieClip
	{
		private var _stageWidth:int;
		private var _swfHolder:Object;
		private var _transHolder:Object;
		private var _vidPlayer:VideoPlayer;
		private var _sectionNum:uint;
		private var _eventHub:EventHub;
		private var _loadCount:uint;
		private var _loadState:String;
		private var _sideLoad:String;
		
		
		/** CONSTRUCTOR
		 * --------------------------------------------------------------------------------------------------- */
		public function SectionSlider ( swfHolder:Object, transHolder:Object )
		{
			_swfHolder = swfHolder;
			_transHolder = transHolder;
			_stageWidth = _swfHolder.stage.stageWidth;
			init ();
		}
		
		
		/** INITIALIZE
		 * --------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			_loadCount = 0;
			_loadState = LoadState.LOAD_IDLE;
			
			// init event hub
			_eventHub = EventHub.getInstance();
			
			// listeners
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
		}
		
		
		/** LOAD CONTENT
		 * --------------------------------------------------------------------------------------------------- */
		private function loadContent ():void
		{
			// load swf & transition
			if (checkForTransition()) loadTransition ();
			loadSWF ();
		}
		
		
		/** CHECK FOR TRANSITION
		 * --------------------------------------------------------------------------------------------------- */
		private function checkForTransition():Boolean
		{
			// check if a transition exists
			if (_loadCount == 1 && SiteSettings.EXPAND_PARAMS.transition)
			{
				return true;
			}
			else if (SiteSettings.PANEL_ARRAY[_sectionNum].transition)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/** LOAD TRANSITION
		 * --------------------------------------------------------------------------------------------------- */
		private function loadTransition ():void
		{
			_swfHolder.visible = false;
			
			// position transition if _sideLoad != null
			switch (_sideLoad)
			{
				case 'left':
					_transHolder.x = -(_transHolder.width) ;
					break;
				case 'right':
					_transHolder.x = _transHolder.width;
					break;
			}
			
			_loadState = LoadState.LOAD_TRANS;
			_transHolder.visible = true;
			
			// get correct video path; if transition found in EXPAND_PARAMS, use this video (intro transition);
			var videoSource:String;
			if (_loadCount == 1 && SiteSettings.EXPAND_PARAMS.transition)
			{
				videoSource = SiteSettings.EXPAND_PARAMS.transition;
			}
			else
			{
				videoSource = SiteSettings.PANEL_ARRAY[_sectionNum].transition;
			}
			
			_vidPlayer = new VideoPlayer ();
			_vidPlayer.attachVideo ( _transHolder, videoSource, true, true );
			
			// add listeners
			_eventHub.addEventListener (VideoEvent.VIDEO_STARTED, slideContent);
			_eventHub.addEventListener (VideoEvent.VIDEO_COMPLETE, playSWF);
		}
		
		
		/** SLIDE CONTENT
		 * --------------------------------------------------------------------------------------------------- */
		private function slideContent (event:VideoEvent):void
		{
			_swfHolder.visible = true;
			var panelLoc:Number;
			var newSWF:Object;
			var swfsLoaded:uint = _swfHolder.numChildren;
			
			if (_sideLoad == 'left')
			{
				
				
				if (swfsLoaded > 0)
				{
					newSWF = _swfHolder.getChildAt(swfsLoaded -1);
					panelLoc = _stageWidth;
				}
				
				else
				{
					panelLoc = _swfHolder.width;
				}
			}
			
			else
			{
				if (swfsLoaded > 0)
				{
					newSWF = _swfHolder.getChildAt(swfsLoaded -1);
					panelLoc = -_stageWidth
				}
				
				else
				{
					panelLoc = -(_swfHolder.width);
				}
			}
			
			TweenNano.to(_swfHolder, .5, {x:panelLoc, onComplete:slideComplete});
			TweenNano.to(_transHolder, .5, {x:0});
		}
		
		
		/** SLIDE COMPLETE
		 * --------------------------------------------------------------------------------------------------- */
		private function slideComplete ():void
		{
			SWFLoader.removeChildren ();
			_swfHolder.x = 0;
			
			var newSWF:Object = _swfHolder.getChildAt(_swfHolder.numChildren -1);
			newSWF.x = 0;
			
			// remove listener
			_eventHub.removeEventListener (VideoEvent.VIDEO_STARTED, slideContent);
		}
		
		
		/** LOAD SWF
		 * --------------------------------------------------------------------------------------------------- */
		private function loadSWF ():void
		{
			// load swf
			var swfSource:String = SiteSettings.PANEL_ARRAY [_sectionNum].swf;
			SWFLoader.loadSWF ({ source:swfSource, container:_swfHolder, callback:swfLoaded, autoPlay:false });
		}
		
		
		/** SWF LOADED
		 * --------------------------------------------------------------------------------------------------- */
		private function swfLoaded ():void
		{
			// position loaded swf
			var newSWF:Object;
			var swfsLoaded:uint = _swfHolder.numChildren;
			
			if (swfsLoaded > 1)
			{
				if (_sideLoad == 'left')
				{
					newSWF = _swfHolder.getChildAt(swfsLoaded -1);
					newSWF.x = -_stageWidth;
				}
				else
				{
					newSWF = _swfHolder.getChildAt(swfsLoaded -1);
					newSWF.x = _stageWidth;
					
				}
			}
			
			// check if transition exists
			if (checkForTransition() == false) playSWF (null);
		}
		
		
		/** PLAY SWF
		 * --------------------------------------------------------------------------------------------------- */
		private function playSWF (event:Event):void
		{
			_eventHub.removeEventListener (VideoEvent.VIDEO_COMPLETE, playSWF);
			
			if (_loadState == LoadState.LOAD_TRANS) 
			{
				// hide video
				_transHolder.visible = false;
				_vidPlayer.removeVideo();
			}
			
			// play swf
			SWFLoader.play();
			if (checkForTransition() == false) slideContent(null);
			_loadState = LoadState.LOAD_SWF;
			_eventHub.dispatchEvent(new Event('show arrows', true));
		}
		
		
		/** CLEAN UP
		 * --------------------------------------------------------------------------------------------------- */
		public function destroy (event:Event):void
		{
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			_eventHub.removeEventListener (VideoEvent.VIDEO_STARTED, slideContent);
			_eventHub.removeEventListener (VideoEvent.VIDEO_COMPLETE, playSWF);
			_vidPlayer = null;
			SWFLoader.unloadSWF();
		}
		
		
		/** PUBLIC - LOAD SECTION
		 * --------------------------------------------------------------------------------------------------- */
		public function loadSection (sectionNum:uint, sideLoad:String):void
		{
			_loadCount ++;
			_sectionNum = sectionNum;
			_sideLoad = sideLoad;
			
			loadContent();
		}
		
		public function unloadSection ():void
		{
			SWFLoader.unloadSWF();
		}
	}
}

// VO class for storing constants used to handle loading states
internal class LoadState
{
	internal static const LOAD_IDLE:String	=		'load idle';
	internal static const LOAD_TRANS:String =		'load transition';
	internal static const LOAD_SWF:String = 		'load swf';
}




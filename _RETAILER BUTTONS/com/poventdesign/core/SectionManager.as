/** SECTION MANAGER
 * ----------------------------------------------------------------------------------------------------------
 * @ description:	handles the loading of swfs & transitions for content
 * @ usage:			new SectionManager ( swfHolder:Object, transHolder:Object, fadeObj:Object );
 * 					loadSection (sectionNum:uint);
 * @ note:			sectionNum refers to the panelArray index found in SiteSettings
 * ---------------------------------------------------------------------------------------------------------- */

package com.poventdesign.core
{
	import com.greensock.TweenNano;
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	import com.poventdesign.loader.SWFLoader;
	import com.poventdesign.ui.video.VideoPlayer;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	import src.SiteSettings;

	
	public class SectionManager extends MovieClip
	{
		private var _swfHolder:Object;
		private var _transHolder:Object;
		private var _fadeObj:Object;
		private var _vidPlayer:VideoPlayer;
		private var _sectionNum:uint;
		private var _eventHub:EventHub;
		private var _loadState:String;
		
		
		/** CONSTRUCTOR
		 * --------------------------------------------------------------------------------------------------- */
		public function SectionManager ( swfHolder:Object, transHolder:Object, fadeObj:Object )
		{
			_swfHolder = swfHolder;
			_transHolder = transHolder;
			_fadeObj = fadeObj;
			
			init ();
		}
		
		
		/** INITIALIZE
		 * --------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			_loadState = LoadState.LOAD_IDLE;
			
			// init event hub
			_eventHub = EventHub.getInstance();
			
			// init fade object
			_fadeObj.alpha = 0;
			_fadeObj.mouseEnabled = false;
			
			// listeners
			_eventHub.addEventListener (VideoEvent.VIDEO_COMPLETE, playSWF);
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
		}
		
		
		/** HANDLE FADE
		 * --------------------------------------------------------------------------------------------------- */
		private function handleFade (value:Boolean):void
		{
			// check if transition exists
			if (checkForTransition() == true)
			{
				// do fade
				if (value) TweenNano.to (_fadeObj, 1, {alpha:.9, onComplete: loadContent});
				else TweenNano.to (_fadeObj, 1, {alpha:0});
			}
			else
			{
				loadSWF ();
			}
		}
		
		
		/** LOAD CONTENT
		 * --------------------------------------------------------------------------------------------------- */
		private function loadContent ():void
		{
			// hide fade
			_fadeObj.visible = false;
			
			// load swf
			loadSWF ();
			
			// load transition
			loadTransition ();
		}
		
		
		private function checkForTransition():Boolean
		{
			// check if a transition exists
			if (SiteSettings._expandCount == 1 && SiteSettings.EXPAND_PARAMS.video)
			{
				return true;
			}
			else if (SiteSettings.PANEL_ARRAY[_sectionNum].transition)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		/** LOAD TRANSITION
		 * --------------------------------------------------------------------------------------------------- */
		private function loadTransition ():void
		{
			// load transition
			_loadState = LoadState.LOAD_TRANS;
			_transHolder.visible = true;
			
			// get correct video path; if video found in EXPAND_PARAMS, use this video (intro video);
			var videoSource:String;
			if (SiteSettings._expandCount == 1 && SiteSettings.EXPAND_PARAMS.video)
			{
				videoSource = SiteSettings.EXPAND_PARAMS.video;
			}
			else
			{
				videoSource = SiteSettings.PANEL_ARRAY[_sectionNum].transition;
			}
			
			_vidPlayer = new VideoPlayer ();
			_vidPlayer.attachVideo ( _transHolder, videoSource, true, true );
		}
		
		
		/** LOAD SWF
		 * --------------------------------------------------------------------------------------------------- */
		private function loadSWF ():void
		{
			// load swf
			var swfSource:String = SiteSettings.PANEL_ARRAY [_sectionNum].swf;
			SWFLoader.loadSWF ({ source:swfSource, container:_swfHolder, callback:swfLoaded, autoPlay:false });
		}
		
		
		/** SWF LOADED
		 * --------------------------------------------------------------------------------------------------- */
		private function swfLoaded ():void
		{
			if (checkForTransition() == false)
			{
				// play swf
				SWFLoader.play();
				_loadState = LoadState.LOAD_SWF;
			}
		}
		
		
		/** PLAY SWF
		 * --------------------------------------------------------------------------------------------------- */
		private function playSWF (event:Event):void
		{
			if (_loadState == LoadState.LOAD_TRANS) 
			{
				// hide video
				_transHolder.visible = false;
				_vidPlayer.removeVideo();
				
				// show fade
				_fadeObj.visible = true;
				handleFade (false);
				
				// play swf
				SWFLoader.play();
				_loadState = LoadState.LOAD_SWF;
			}
		}
		
		
		/** CLEAN UP
		 * --------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			_eventHub.removeEventListener (VideoEvent.VIDEO_COMPLETE, playSWF);
			_vidPlayer = null;
			SWFLoader.unloadSWF();
		}
		
		
		/** PUBLIC - LOAD SECTION
		 * --------------------------------------------------------------------------------------------------- */
		public function loadSection (sectionNum:uint):void
		{
			SiteSettings._expandCount ++;
			_sectionNum = sectionNum;
			handleFade (true);
		}
		
		public function unloadSection ():void
		{
			SWFLoader.unloadSWF();
		}
	}
}

// VO class for storing constants used to handle loading states
internal class LoadState
{
	internal static const LOAD_IDLE:String	=		'load idle';
	internal static const LOAD_TRANS:String =		'load transition';
	internal static const LOAD_SWF:String = 		'load swf';
}




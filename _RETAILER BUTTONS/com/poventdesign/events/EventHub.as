/** EVENT HUB
 * ----------------------------------------------------------------
 * @ description: central hub for event listeners & dispatchers
 * ---------------------------------------------------------------- */

package com.poventdesign.events
{
	import flash.events.*;
	
	public class EventHub extends EventDispatcher {
		private static var _instance:EventHub;
		
		public function EventHub(enforcer:SingletonEnforcer):void
		{
			super();
		}
		
		public static function getInstance():EventHub
		{
			if (_instance == null)
				_instance = new EventHub( new SingletonEnforcer() );
			
			return _instance;
		}
		
		public override function dispatchEvent(event:Event):Boolean
		{
			return super.dispatchEvent(event);
		}
	}
}

// singleton enforcer
class SingletonEnforcer {}
/** DICTIONARY UTILITIES
 * ---------------------------------------------------------------------
 * @ description: utility class for common dictionary functions
 * --------------------------------------------------------------------- */

package poventdesign.utils
{
	import flash.utils.Dictionary;

	public class DictionaryUtils
	{
		// returns total keys in passed dictionary
		public static function getDictionaryLength (dict:Dictionary):uint
			{
				var totalKeys:uint = 0;
				for (var key:* in dict) {
					totalKeys ++;
				}
				
				return totalKeys;
			}
		}
	}
}
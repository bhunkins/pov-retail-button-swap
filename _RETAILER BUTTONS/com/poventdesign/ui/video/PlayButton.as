/** PLAY VIDEO BUTTON
 * ------------------------------------------------------------------------------------------------------------------------ *
 * @ description:	play button for use with com.poventdesign.ui.video.VideoPlayer
 * @ usage:			buttonInstance.init(videoPlayerInstance);
 * ------------------------------------------------------------------------------------------------------------------------ */

package com.poventdesign.ui.video
{
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Video;
	
	public class PlayButton extends MovieClip
	{
		private var _eventHub:EventHub;
		private var _isPaused:Boolean;
		
		
		/** CONSTRUCTOR
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function PlayButton ()
		{
			// event hub reference
			_eventHub = EventHub.getInstance();
			init ();
		}
		
		
		/** INITIALIZE
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function init ():void
		{
			// clean up
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.addEventListener (VideoEvent.VIDEO_PAUSED, enable);
			_eventHub.addEventListener (VideoEvent.VIDEO_RESUMED, disable);
			_eventHub.addEventListener (VideoEvent.VIDEO_STARTED, disable);
		}
		
		
		/** ENABLE
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function enable (event:VideoEvent):void
		{
			this.visible = true;
			this.buttonMode = true;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
			addEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}	
		
		
		/** DISABLE
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function disable (event:VideoEvent):void
		{
			this.visible = false;
			this.buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
			
		
		/** MOUSE HANDLER
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			switch (event.type)
			{
				case 'rollOver':
					event.target.alpha = .7;
					break;
				case 'rollOut':
					event.target.alpha = 1;
					break;
				case 'mouseUp':
					if (_isPaused) _eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_UNPAUSE));
					else _eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_PAUSE));
					break;
			}
		}
		
		
		/** UPDATE ICON
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function updateIcon (event:Event):void
		{
			if (event.type == VideoEvent.VIDEO_RESUMED || event.type == VideoEvent.VIDEO_STARTED)
			{
				this.visible = false;
				_isPaused = false;
			}
				
			else if (event.type == VideoEvent.VIDEO_PAUSED || event.type == VideoEvent.VIDEO_COMPLETE)
			{
				this.visible = true;
				_isPaused = true;
			}
		}
		
		
		/** CLEAN UP
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			
			// clean up
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.removeEventListener (VideoEvent.VIDEO_PAUSED, enable);
			_eventHub.removeEventListener (VideoEvent.VIDEO_RESUMED, disable);
			_eventHub.removeEventListener (VideoEvent.VIDEO_STARTED, disable);
			
			// button events
			this.buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
	}
}

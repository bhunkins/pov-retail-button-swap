/** REPLAY VIDEO BUTTON
 * ------------------------------------------------------------------------------------------------------------------------ *
 * @ description:	replay button for use with com.poventdesign.ui.video.VideoPlayer
 * @ usage:			attach to a MovieClip's base class
 * ------------------------------------------------------------------------------------------------------------------------ */

package com.poventdesign.ui.video
{
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class ReplayVideo extends MovieClip
	{
		private var _eventHub:EventHub;
		
		
		/** CONSTRUCTOR
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function ReplayVideo ()
		{
			// event hub reference
			_eventHub = EventHub.getInstance ();
			
			// initialize the button
			init ();
		}
		
		
		/** INITIALIZE
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			// clean up
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.addEventListener ( VideoEvent.VIDEO_COMPLETE, this.enable );
			_eventHub.addEventListener ( VideoEvent.VIDEO_STARTED, this.disable );
			
			// setup
			disable ();
		}
	
		
		/** MOUSE HANDLER
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			switch (event.type)
			{
				case 'rollOver':
					this.alpha = .7;
					break;
				case 'rollOut':
					this.alpha = 1;
					break;
				case 'mouseUp':
					_eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_REPLAY));
					break;
			}
		}
		
		
		/** ENABLE BUTTON
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function enable (event:VideoEvent = null):void
		{
			this.visible = true;
			buttonMode = true;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
			addEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** DISABLE BUTTON
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function disable (event:VideoEvent = null):void
		{
			this.visible = false;
			buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** CLEAN UP
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			// clean up
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.removeEventListener ( VideoEvent.VIDEO_COMPLETE, this.enable );
			_eventHub.removeEventListener ( VideoEvent.VIDEO_STARTED, this.disable );
			
			// button events
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
	}
}
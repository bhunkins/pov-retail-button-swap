/** PAUSE/PLAY VIDEO BUTTON
 * ------------------------------------------------------------------------------------------------------------------------ *
 * @ description:	pause/play button for use with com.poventdesign.ui.video.VideoPlayer
 * @ usage:			attach to a MovieClip's base class
 * ------------------------------------------------------------------------------------------------------------------------ */

package com.poventdesign.ui.video
{
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class PlayPauseButton extends MovieClip
	{
		private var _eventHub:EventHub;
		private var _isPaused:Boolean;
		
		
		/** CONSTRUCTOR
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function PlayPauseButton ()
		{
			// event hub reference
			_eventHub = EventHub.getInstance();
			init ();
		}
		
		
		/** INITIALIZE
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			// clean up
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video event listeners
			_eventHub.addEventListener (VideoEvent.VIDEO_PAUSED, updateIcon);
			_eventHub.addEventListener (VideoEvent.VIDEO_RESUMED, updateIcon);
			_eventHub.addEventListener (VideoEvent.VIDEO_STARTED, updateIcon);
			_eventHub.addEventListener (VideoEvent.VIDEO_COMPLETE, updateIcon);
			
			// button event listeners
			this.buttonMode = true;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
			addEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** MOUSE HANDLER
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			switch (event.type)
			{
				case 'rollOver':
					this.alpha = .7;
					break;
				
				case 'rollOut':
					this.alpha = 1;
					break;
				
				case 'mouseUp':
					if (_isPaused) _eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_UNPAUSE));
					else _eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_PAUSE));
					break;
			}
		}
		
		
		/** UPDATE ICON - called from VideoEvent
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function updateIcon (event:VideoEvent):void
		{
			if (event.type == VideoEvent.VIDEO_RESUMED || event.type == VideoEvent.VIDEO_STARTED)
			{
				this.gotoAndStop('pause');
				_isPaused = false;
			}
			
			else if (event.type == VideoEvent.VIDEO_PAUSED || event.type == VideoEvent.VIDEO_COMPLETE)
			{
				this.gotoAndStop('play');
				_isPaused = true;
			}
		}
		
		
		/** CLEAN UP
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			// clean up
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video event listeners
			_eventHub.removeEventListener (VideoEvent.VIDEO_PAUSED, updateIcon);
			_eventHub.removeEventListener (VideoEvent.VIDEO_RESUMED, updateIcon);
			_eventHub.removeEventListener (VideoEvent.VIDEO_STARTED, updateIcon);
			_eventHub.removeEventListener (VideoEvent.VIDEO_COMPLETE, updateIcon);
			
			// button event listeners
			this.buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
	}
}

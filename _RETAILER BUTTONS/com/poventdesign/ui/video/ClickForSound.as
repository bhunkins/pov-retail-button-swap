/** CLICK FOR SOUND
 * ------------------------------------------------------------------------------------------------------------------------ *
 * @ description:	click for sound button for use with com.poventdesign.ui.video.VideoPlayer
 * @ usage:			attach to a MovieClip's base class
 * ------------------------------------------------------------------------------------------------------------------------ */

package com.poventdesign.ui.video
{
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class ClickForSound extends MovieClip
	{
		private var _eventHub:EventHub;
		
		
		/** CONSTRUCTOR
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function ClickForSound ()
		{
			// event hub reference
			_eventHub = EventHub.getInstance();
			addListeners();
		}
		
		/** ADD LISTENERS
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function addListeners ():void
		{
			// clean up
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.addEventListener (VideoEvent.VIDEO_SOUND_OFF, update);
			_eventHub.addEventListener (VideoEvent.VIDEO_SOUND_ON, update);
			_eventHub.addEventListener (VideoEvent.VIDEO_PAUSED, update);
			_eventHub.addEventListener (VideoEvent.VIDEO_COMPLETE, update);
			
			// button events
			this.buttonMode = true;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
			addEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** REMOVE LISTENERS
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function removeListeners ():void
		{
			// clean up
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.removeEventListener (VideoEvent.VIDEO_SOUND_OFF, update);
			_eventHub.removeEventListener (VideoEvent.VIDEO_SOUND_ON, update);
			_eventHub.removeEventListener (VideoEvent.VIDEO_PAUSED, update);
			_eventHub.removeEventListener (VideoEvent.VIDEO_COMPLETE, update);
			
			// button events
			this.buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** MOUSE HANDLER
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			switch (event.type)
			{
				case 'rollOver':
					this.alpha = .7;
					break;
				case 'rollOut':
					this.alpha = 1;
					break;
				case 'mouseUp':
					_eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_UNMUTE));
					removeListeners();
					break;
			}
		}
		
		
		/** UPDATE DISPLAY
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function update (event:Event = null):void
		{
			switch(event.type)
			{
				case VideoEvent.VIDEO_SOUND_OFF:
					this.visible = true;
					break;
				
				default:
					this.visible = false;
					removeListeners();
			}
		}
		
		
		/** CLEAN UP
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			removeListeners();
		}
		
		
	}
}

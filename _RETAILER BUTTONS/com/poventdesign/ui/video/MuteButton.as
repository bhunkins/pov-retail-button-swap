/** MUTE VIDEO BUTTON
 * ------------------------------------------------------------------------------------------------------------------------ *
 * @ description:	mute button for use with com.poventdesign.ui.video.VideoPlayer
 * @ usage:			attach to a MovieClip's base class;
 * ------------------------------------------------------------------------------------------------------------------------ */

package com.poventdesign.ui.video
{
	import com.poventdesign.events.EventHub;
	import com.poventdesign.events.VideoEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MuteButton extends MovieClip
	{
		private var _eventHub:EventHub;
		private var _isMuted:Boolean
		
		
		/** CONSTRUCTOR
		 * ---------------------------------------------------------------------------------------------------------------- */
		public function MuteButton ()
		{
			// event hub reference
			_eventHub = EventHub.getInstance();
			init ();
		}
		
		
		/** INITIALIZE
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			// cleanup
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video event listeners
			_eventHub.addEventListener (VideoEvent.VIDEO_SOUND_OFF, updateIcon);
			_eventHub.addEventListener (VideoEvent.VIDEO_SOUND_ON, updateIcon);
			
			// button events
			this.buttonMode = true;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
			addEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
		/** MOUSE HANDLER
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			switch (event.type)
			{
				case 'rollOver':
					this.alpha = .7;
					break;
				
				case 'rollOut':
					this.alpha = 1;
					break;
				
				case 'mouseUp':
					if (_isMuted)
					{
						_eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_UNMUTE));
					}
					else
					{
						_eventHub.dispatchEvent (new Event (VideoEvent.SET_VIDEO_MUTE));
					}
					break;
			}
		}
		
		
		/** UPDATE ICON - called from VideoEvent
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function updateIcon (event:VideoEvent):void
		{
			if (event.type == VideoEvent.VIDEO_SOUND_OFF)
			{
				// set to mute
				_isMuted = true;
				this.gotoAndStop('mute');
			}
				
			else if (event.type == VideoEvent.VIDEO_SOUND_ON)
			{
				// set to unmute
				_isMuted = false;
				this.gotoAndStop('unmute');
			}
		}
		
		
		/** CLEAN UP
		 * ---------------------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			// cleanup
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// video events
			_eventHub.removeEventListener (VideoEvent.VIDEO_SOUND_OFF, updateIcon);
			_eventHub.removeEventListener (VideoEvent.VIDEO_SOUND_ON, updateIcon);
			
			// button events
			this.buttonMode = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
			removeEventListener (MouseEvent.MOUSE_UP, handleMouse);
		}
		
		
	}
}

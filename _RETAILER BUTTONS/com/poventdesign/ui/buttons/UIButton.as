/** UI BUTTON CLASS
 *  ----------------------------------------------------------------------------------------------------------
 *  @ description:	handles mouse events for buttons
 *  @ usage:		assign as the base class for a MovieClip in the library
 *  ---------------------------------------------------------------------------------------------------------- */

package com.poventdesign.ui.buttons
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class UIButton extends MovieClip
	{
		
		/** CONSTRUCT
		 * --------------------------------------------------------------------------------------------------- */
		public function UIButton ()
		{
			super ();
			addListeners ();
		}

		
		/** ADD LISTENERS
		 * --------------------------------------------------------------------------------------------------- */
		private function addListeners ():void
		{
			// garbage collection
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// button functionality
			enabled = true;
			buttonMode = true;
			mouseEnabled = true;
			mouseChildren = false;
			addEventListener (MouseEvent.ROLL_OVER, handleMouse);
			addEventListener (MouseEvent.ROLL_OUT, handleMouse);
		}
		
		
		/** REMOVE LISTENERS
		 * --------------------------------------------------------------------------------------------------- */
		private function removeListeners ():void
		{
			// garbage collection
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// button functionality
			enabled = false;
			buttonMode = false;
			mouseEnabled = false;
			removeEventListener (MouseEvent.ROLL_OVER, handleMouse);
			removeEventListener (MouseEvent.ROLL_OUT, handleMouse);
		}
		
		
		/** HANDLE MOUSE
		 * --------------------------------------------------------------------------------------------------- */
		private function handleMouse (event:MouseEvent):void
		{
			if (enabled)
			{
				switch (event.type)
				{
					case 'rollOver':
						this.gotoAndPlay ('over');
						break;
					case 'rollOut':
						this.gotoAndPlay ('out');
						break;
				}
			}
		}
		
		protected function enableButton (value:Boolean):void
		{
			value ? addListeners() : removeListeners();
		}
		
		
		/** CLEAN UP
		 * --------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			removeListeners ();
		}
		
		
	}
}


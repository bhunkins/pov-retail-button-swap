/** STATE BUTTON CLASS
 *  ----------------------------------------------------------------------------------------------------------
 *  @ description:	state button class
 *  @ usage:		assign as the base class for a MovieClip in the library, then set the parameters
 *  @ example:		buttonInstance.params = {id:uint, group:String};
 *  @ params:		id: number to match PANEL_ARRAY index that this button corresponds to
 * 					group: name of group this button belongs to; used to alleviate conflicts   
 *  ---------------------------------------------------------------------------------------------------------- */

package com.poventdesign.ui.buttons
{
	import com.poventdesign.events.CustomEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	
	
	public class StateButton extends UIButton
	{
		// constants
		private const UPDATE_STATE_BUTTON:String = 'update state button';
		
		// vars
		private var _selected:Boolean;
		private var _id:uint;
		private var _group:String;
		
		
		/** CONSTRUCTOR
		 * --------------------------------------------------------------------------------------------------- */
		public function StateButton ()
		{
			// setup
			super ();
			_selected = false;
			stage ? init (null) : addEventListener (Event.ADDED_TO_STAGE, init);
		}
		
		private function init (event:Event):void
		{
			if (event) removeEventListener (Event.ADDED_TO_STAGE, init);
			addListeners ();
		}
		
		
		/** ADD LISTENERS
		 * --------------------------------------------------------------------------------------------------- */
		private function addListeners ():void
		{
			// garbage collection
			addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// mouse event
			enabled = true;
			mouseEnabled = true;
			addEventListener (MouseEvent.MOUSE_UP, selectButton);
			
			// for updating state buttons
			stage.addEventListener (UPDATE_STATE_BUTTON, updateNav);
		}
		
		
		/** REMOVE LISTENERS
		 * --------------------------------------------------------------------------------------------------- */
		private function removeListeners ():void
		{
			// garbage collection
			removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// mouse event
			enabled = false;
			mouseEnabled = false;
			removeEventListener (MouseEvent.MOUSE_UP, selectButton);
			
			// for updating state buttons
			stage.removeEventListener (UPDATE_STATE_BUTTON, updateNav);
		}
		
		
		/** SELECT BUTTON
		 * --------------------------------------------------------------------------------------------------- */
		private function selectButton (event:MouseEvent):void
		{
			dispatchEvent ( new CustomEvent (UPDATE_STATE_BUTTON, this, true) );
		}
		
		
		/** UPDATE NAV BUTTON
		 * --------------------------------------------------------------------------------------------------- */
		private function updateNav (event:CustomEvent):void
		{
			var selectedBtn:Object = event.params as Object;
			
			// only affect the same button group
			if (selectedBtn.group == this.group)
			{
				if (selectedBtn == this)
				{
					if (enabled) this.gotoAndPlay ('active');
					_selected = true;
					enabled = false;
					mouseEnabled = false;
					super.enableButton (false);
				}
					
				else
				{
					_selected = false;
					enabled = true;
					mouseEnabled = true;
					this.gotoAndPlay ('idle');
					super.enableButton (true);
				}
			}
		}
		
		
		/** CLEAN UP
		 * --------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			removeListeners ();
		}
		
		
		/** PUBLIC METHODS
		 * --------------------------------------------------------------------------------------------------- */
		// getSelected
		public function getSelected ():Boolean
		{
			return _selected;
		}

		// setSelected
		public function setSelected ():void
		{
			this.gotoAndPlay ('active');
			_selected = true;
			enabled = false;
			super.enableButton (false);
			
			// dispatch event for other buttons
			selectButton (null);
		}
		
		// get id
		public function get id ():uint
		{
			return _id;
		}
		
		// get group
		public function get group ():String
		{
			return _group;
		}
		
		// set params
		public function set params (obj:Object):void
		{
			_id = obj.id;
			_group = obj.group;
		}
			
	}
}


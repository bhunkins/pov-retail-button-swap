/** PHOTO GALLERY CLASS
 *  ----------------------------------------------------------------------------------------------------------
 *  @ description: photo gallery with fades using TweenNano
 *  @ usage: new PhotoGallery ( sourceArray:Array, imageHolder:Object, arrowBack:Object, arrowNext:Object );
 *  ---------------------------------------------------------------------------------------------------------- */

package com.poventdesign.ui.gallery
{
	import com.greensock.TweenNano;
	import com.poventdesign.loader.ContentLoader;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class PhotoGallery extends MovieClip
	{
		private var _sourceArray:Array;
		private var _imageHolder:Object;
		private var _arrowBack:Object;
		private var _arrowNext:Object;
		private var _currImage:uint;
		
		
		/** CONSTRUCT
		 * --------------------------------------------------------------------------------------------------- */
		public function PhotoGallery ( sourceArray:Array, imageHolder:Object, arrowBack:Object, arrowNext:Object )
		{
			// components
			_sourceArray = sourceArray;
			_imageHolder = imageHolder;
			_arrowBack = arrowBack;
			_arrowNext = arrowNext;
			
			init();
		}
		
		
		/** INITIALIZE PHOTO GALLERY
		 * --------------------------------------------------------------------------------------------------- */
		private function init ():void
		{
			_currImage = 1;
			loadImage();
			updateNav();
			
			// add listeners
			this.addEventListener (Event.REMOVED_FROM_STAGE, destroy);
			_arrowBack.addEventListener (MouseEvent.MOUSE_UP, switchImage);
			_arrowNext.addEventListener (MouseEvent.MOUSE_UP, switchImage);
		}
		
		
		/** UPDATE NAV -- hides/shows arrow buttons
		 * --------------------------------------------------------------------------------------------------- */
		private function updateNav ():void
		{
			// back button
			_currImage == 1 ? _arrowBack.visible = false : _arrowBack.visible = true;
			
			// next button
			var totalImages:uint = _sourceArray.length;
			_currImage == totalImages ? _arrowNext.visible = false : _arrowNext.visible = true;
		}
		
		
		/** SWITCH IMAGE
		 * --------------------------------------------------------------------------------------------------- */
		private function switchImage (event:MouseEvent):void
		{
			switch (event.currentTarget)
			{
				case _arrowBack:
					_currImage --;
					updateNav ();
					break;
				case _arrowNext:
					_currImage ++;
					updateNav ();
					break;
			}
			
			// fade out
			TweenNano.to (_imageHolder, .5, {alpha:0, onComplete:loadImage});	
		}
		
		
		/** LOAD IMAGE -- uses ContentLoader class
		 * --------------------------------------------------------------------------------------------------- */
		private function loadImage ():void
		{
			var nextImage:String = _sourceArray[_currImage -1];
			ContentLoader.loadContent ( nextImage, loadComplete );
		}
		
		
		/** LOAD COMPLETE
		 * --------------------------------------------------------------------------------------------------- */
		private function loadComplete (loader:Loader):void
		{
			var totalContent:uint = _imageHolder.numChildren;
			
			//if (totalContent > 1) _imageHolder.removeChildAt(totalContent -1);
			_imageHolder.addChild (loader);
			
			// fade in
			TweenNano.to (_imageHolder, .5, {alpha:1});
		}
		
		
		/** CLEAN UP
		 * --------------------------------------------------------------------------------------------------- */
		private function destroy (event:Event):void
		{
			this.removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			_arrowBack.removeEventListener (MouseEvent.MOUSE_UP, switchImage);
			_arrowNext.removeEventListener (MouseEvent.MOUSE_UP, switchImage);
		}
	}
}

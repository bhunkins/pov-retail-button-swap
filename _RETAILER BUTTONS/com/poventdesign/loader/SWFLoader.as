/** SWF LOADER
 * ----------------------------------------------------------------------------------------------
 * @ description: loads SWFs from source path and returns the content as loader
 * @ usage: loadSWF({parameters});
 * @ params include: sourcePath:String, container:MovieClip, callback:Function, autoPlay:Boolean
 * @ developer: Brian Hunkins : brian@poventdesign.com
 * @ version: 1.0  6/28/13
 * ------------------------------------------------------------------------------------------- */

package com.poventdesign.loader
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	public class SWFLoader extends Loader
	{
		private static var _loader:Loader;
		private static var _loadedSWF:MovieClip;
		
		// parameters
		private static var _sourcePath:String;
		private static var _container:MovieClip;
		private static var _callback:Function;
		private static var _autoPlay:Boolean;
		
		
		/** PRIVATE METHODS
		 * ----------------------------------------------------------------------------------- */
		private static function loadInit (event:Event):void
		{
			_loadedSWF = _loader.content as MovieClip;
			
			// determine autoPlay
			_autoPlay ? _loadedSWF.play() : _loadedSWF.stop();
			
			//if (_container.numChildren > 0) _container.removeChildAt(0);
			_container.addChild(_loadedSWF);
			
			// run callback function, if exists
			_callback();
		}
		
		
		/** ERROR HANDLER
		 * ----------------------------------------------------------------------------------- */
		private static function errorHandler (event:IOErrorEvent):void
		{
			trace('<<< ERROR LOADING SWF:   ' + _sourcePath + '   ... PLEASE CHECK THE SOURCE PATH >>>');
		}
		
		
		/** CLEAN UP
		 * ----------------------------------------------------------------------------------- */
		private static function destroy (event:Event):void
		{
			// remove listeners
			_loader.contentLoaderInfo.removeEventListener (Event.INIT, loadInit);
			_loader.contentLoaderInfo.removeEventListener (IOErrorEvent.IO_ERROR, errorHandler);
			_loader.removeEventListener (Event.REMOVED_FROM_STAGE, destroy);
			
			// dispose of loaded swf
			_loader.unload();
			_loader = null;
			_loadedSWF = null;
		}

		
		/** PUBLIC METHODS
		 * ----------------------------------------------------------------------------------- */
		public static function loadSWF (params:Object):void
		{
			// store params
			_sourcePath = params.source;
			_container = params.container;
			_callback = params.callback;
			_autoPlay = params.autoPlay;
			
			// load source
			var req:URLRequest = new URLRequest (_sourcePath);
			_loader = new Loader();
			_loader.load (req);
			
			// listeners
			_loader.contentLoaderInfo.addEventListener (Event.INIT, loadInit);
			_loader.contentLoaderInfo.addEventListener (IOErrorEvent.IO_ERROR, errorHandler);
			_loader.addEventListener (Event.REMOVED_FROM_STAGE, destroy);
		}
		
		public static function unloadSWF ():void
		{
			destroy (null);
		}
		
		public static function removeChildren ():void
		{
			if (_container.numChildren > 1) _container.removeChildAt(0);
		}
		
		// play swf
		public static function play ():void
		{
			_loadedSWF.play();
		}
		
		// stop swf
		public static function stop ():void
		{
			_loadedSWF.stop();
		}
		
		
	}
	
}
